import fitz # imports the pymupdf library
import rich
import json


def flags_decomposer(flags):
    """Make font flags human readable."""
    l = []
    if flags & 2 ** 0:
        l.append("superscript")
    if flags & 2 ** 1:
        l.append("italic")
    if flags & 2 ** 2:
        l.append("serifed")
    else:
        l.append("sans")
    if flags & 2 ** 3:
        l.append("monospaced")
    else:
        l.append("proportional")
    if flags & 2 ** 4:
        l.append("bold")
    return ", ".join(l)


results = []
image_index = 0
doc = fitz.open("./input/pdf_mock_file.pdf") # open a document
new_doc = fitz.Document()
for index, page in enumerate(doc): # iterate the document pages
    new_page = new_doc.new_page()
    text_dict = page.get_text("dict", flags=fitz.TEXTFLAGS_DICT)
    blocks = text_dict["blocks"]
    for b in blocks:  # iterate through the text blocks
        if b["type"] == 0: # text block
            for l in b["lines"]:  # iterate through the text lines
                for s in l["spans"]:  # iterate through the text spans
                    print(f"\nText: {s['text']}\nFont: {s['font']}\nColor: {s['color']}\nSize: {s['size']}\nStyle: {flags_decomposer(s['flags'])}")  # simple print of text

                    results.append({
                        "text": s["text"],
                        "font": s["font"],
                        "color": fitz.sRGB_to_rgb(s["color"]),
                        "size": s["size"],
                        "style": flags_decomposer(s['flags']),
                    })

                    writer = fitz.TextWriter(new_page.rect, color=fitz.sRGB_to_pdf(s["color"]))
                    is_bold = "bold" in flags_decomposer(s["flags"])
                    is_italic = "italic" in flags_decomposer(s["flags"])
                    font_name = s["font"]
                    style = ""
                    if is_bold: style += "Bold" 
                    if is_italic: style += "Italic"
                    if style: style = "-" + style
                    try:
                        font = fitz.Font(font_name + style)
                    except Exception as e:
                        font_name = "Times" + ("-Roman" if not style else style)
                        font = fitz.Font(font_name)
                    writer.append(s["origin"], text=s["text"].upper(), fontsize=s["size"], font=font)
                    new_page.write_text(rect=fitz.Rect(*s["bbox"]), writers=writer)



    images = doc.get_page_images(index)
    for img in images:
        xref = img[0]
        pix = fitz.Pixmap(doc, xref)
        rect = page.get_image_rects(img)
        pix.save(f"output/pdf_mock_file/img{image_index:02d}.png")
        image_index += 1
        new_page.insert_image(rect=fitz.Rect(*rect), pixmap=pix)


json.dump(results, open("output/pdf_mock_file/pdf_mock_file_text.json", "w+"))
new_doc.save("output/pdf_mock_file/pdf_mock_file_upper.pdf")

    