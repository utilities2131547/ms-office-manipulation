import rich
import json
from docx import Document


document = Document("./input/docx_mock_file.docx")
results = []

def parse_style(run):
    font = run.font
    text = run.text
    font_name = font.name
    font_size = font.size
    font_color = font.color.rgb
    font_style = []
    if font.bold:
        font_style.append("bold")
    if font.italic:
        font_style.append("italic")

    repr = f"\nText: {text}\nFont: {font_name}\nSize: {font_size}\nColor: {font_color}\nStyle: {','.join(font_style)}"
    results.append({
        "text": text,
        "font": font_name,
        "size": font_size,
        "color": font_color,
        "style": font_style
    })
    return repr



for paragraph in document.paragraphs:
    runs = paragraph.runs
    for run in runs:
        print(parse_style(run))
        run.text = run.text.upper()

for table in document.tables:
    for row_index in range(len(table.rows)):
        for col_index in range(len(table.columns)):
            cell = table.cell(row_index, col_index)
            for paragraph in cell.paragraphs:
                runs = paragraph.runs
                for run in runs:
                    print(parse_style(run))
                    run.text = run.text.upper()


document.save("output/docx_mock_file_upper.docx")
json.dump(results, open("output/docx_mock_file_text.json", "w+"))