import os
import json
from pptx import Presentation
from pptx.shapes.placeholder import SlidePlaceholder
from pptx.shapes.picture import Picture
from pptx.shapes.connector import Connector
from pptx.shapes.autoshape import Shape
from deep_translator import GoogleTranslator



def translate(text):
    translated = GoogleTranslator(source='en', target='vi').translate(text) 
    return translated


results = []
ppt = Presentation("./input/Networking.pptx")
image_index = 0
for slide_index, slide in enumerate(ppt.slides):
    for shape_index, shape in enumerate(slide.shapes):
        if isinstance(shape, SlidePlaceholder):
            results.append({"text": shape.text})
            lines = shape.text.split("\n")
            join_line = "\n".join([f"{line}\n{translate(line)}" for line in lines])
            for line in lines:
                shape.text = join_line
        elif isinstance(shape, Picture):
            image = shape.image
            image_bytes = image.blob
            # ---make up a name for the file, e.g. 'image.jpg'---
            image_filename = f"output/networking/image_{image_index:02d}.{image.ext}"
            with open(image_filename, 'wb') as f:
                f.write(image_bytes)
            image_index += 1

        elif isinstance(shape, Connector):
            print(shape)
        elif isinstance(shape, Shape):
            results.append({"text": shape.text})
            lines = shape.text.split("\n")
            join_line = "\n".join([f"{line}\n{translate(line)}" for line in lines])
            for line in lines:
                shape.text = join_line
        else:
            raise Exception("unknow type")

json.dump(results, open("output/networking/text.json", "w+"))
ppt.save("output/networking/Networking_translate.pptx")